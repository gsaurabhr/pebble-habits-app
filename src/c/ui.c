#include <pebble.h>
#include "ui.h"
#include "appmsg.h"
#include "storage.h"
#include "habits.h"

static Window *s_window;
static MenuLayer *s_menu_layer;
//static TextLayer *s_text_layer;
static int s_num_default = 5;
static char s_msg_buf[200];
static app_msg s_msg = {.key = -1, .payload=s_msg_buf};
static activity s_act = {.key=-1, .start_time=-1, .end_time=-1};

static uint16_t cb_get_menu_section_count(struct MenuLayer *menu_layer,
                                          void *callback_context) {
  if (get_running_count() > 0) {return 2;}
  return 0;
}

static uint16_t cb_get_menu_row_count(struct MenuLayer *menu_layer,
                                           uint16_t section_index,
                                           void *callback_context) {
  if ((get_running_count() > 0) & (section_index == 0)) {return get_running_count();}
  else {return s_num_default;}
}

static int16_t cb_get_menu_header_height(struct MenuLayer *menu_layer,
                                         uint16_t section_index,
                                         void *callback_context) {
  return 20;
}

static int16_t cb_get_menu_cell_height(struct MenuLayer *menu_layer,
                                       MenuIndex *cell_index,
                                       void *callback_context) {
  if ((get_running_count() > 0) & (cell_index->section == 0)) {return 40;}
  else {return 24;}
  return 0;
}

static void cb_menu_draw_row(GContext *ctx, const Layer *cell_layer,
                                  MenuIndex *cell_index,
                                  void *callback_context) {
  if ((get_running_count() > 0) & (cell_index->section == 0)) {
    load_activity(cell_index->row, &s_act, false);
    int tdiff = time(NULL)-s_act.start_time;
    char buffer[20];
    snprintf(buffer, sizeof(buffer), "%2d h %2d m", tdiff/3600, (tdiff%3600)/60);
    menu_cell_basic_draw(ctx, cell_layer, act_names[s_act.key], buffer, NULL);
  }
  else {
    menu_cell_basic_draw(ctx, cell_layer, act_names[cell_index->row], NULL, NULL);
  }
}

static void cb_menu_draw_header(GContext *ctx, const Layer *cell_layer,
                                uint16_t section_index,
                                void *callback_context) {
  if ((get_running_count() > 0) & (section_index == 0)) {
    menu_cell_basic_header_draw(ctx, cell_layer, "Currently running");
  }
  else {
    menu_cell_basic_header_draw(ctx, cell_layer, "All Activities");
  }
}

static void cb_menu_select(struct MenuLayer *menu_layer,
                                MenuIndex *cell_index,
                                void *callback_context) {
  if ((get_running_count() > 0) & (cell_index->section == 0)) {
    load_activity(cell_index->row, &s_act, true);
    s_msg.key = MESSAGE_KEY_ACTIVITY;
    snprintf(s_msg_buf, sizeof(s_msg_buf), "%s,%d,%d", act_names[s_act.key], (int)s_act.start_time, (int)time(NULL));
    APP_LOG(APP_LOG_LEVEL_DEBUG, s_msg.payload);
    send_to_phone(&s_msg);
  }
  else {
    s_act.key = cell_index->row;
    s_act.start_time = time(NULL);
    save_activity(&s_act);
  }
}

static void cb_menu_select_short(struct MenuLayer *menu_layer,
                                MenuIndex *cell_index,
                                void *callback_context) {
  cb_menu_select(menu_layer, cell_index, callback_context);
  menu_layer_reload_data(s_menu_layer);
}

static void cb_menu_select_long(struct MenuLayer *menu_layer,
                                MenuIndex *cell_index,
                                void *callback_context) {
  cb_menu_select(menu_layer, cell_index, callback_context);
  window_stack_pop_all(true);
}

static void cb_menu_min_tick(struct tm *tick_time, TimeUnits units_changed) {
  menu_layer_reload_data(s_menu_layer);
}

/******************************************************************************/

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  s_menu_layer = menu_layer_create(GRect(0, 0, bounds.size.w, bounds.size.h));
  menu_layer_set_callbacks(s_menu_layer, NULL, (MenuLayerCallbacks){
    .get_num_sections = cb_get_menu_section_count,
    .get_num_rows = cb_get_menu_row_count,
    .get_header_height = cb_get_menu_header_height,
    .get_cell_height = cb_get_menu_cell_height,
    .draw_row = cb_menu_draw_row,
    .draw_header = cb_menu_draw_header,
    .select_click = cb_menu_select_short,
    .select_long_click = cb_menu_select_long
  }); 
  menu_layer_set_click_config_onto_window(s_menu_layer, window);
  menu_layer_set_normal_colors(s_menu_layer, GColorBlack, GColorClear);
  menu_layer_set_highlight_colors(s_menu_layer, GColorWhite, GColorBlack);
  //force_back_button(window, s_main_menu_layer);
  layer_add_child(window_layer, menu_layer_get_layer(s_menu_layer));
  tick_timer_service_subscribe(MINUTE_UNIT, cb_menu_min_tick);
  /*s_text_layer = text_layer_create(GRect(0, 72, bounds.size.w, 20));
  text_layer_set_text(s_text_layer, "Press a button");
  text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(s_text_layer));*/
}

static void window_unload(Window *window) {
  tick_timer_service_unsubscribe();
  menu_layer_destroy(s_menu_layer);
  //text_layer_destroy(s_text_layer);
}

void ui_init() {
  s_window = window_create();
  //window_set_click_config_provider(s_window, click_config_provider);
  window_set_window_handlers(s_window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(s_window, animated);
}

void ui_deinit() {
  window_destroy(s_window);
}
