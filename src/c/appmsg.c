#include <pebble.h>
#include "appmsg.h"
#include "storage.h"

static bool s_js_ready = false;

static void inbox_received_callback(DictionaryIterator *iterator, void *context) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Inbox received successfully");
  Tuple *ready_tuple = dict_find(iterator, MESSAGE_KEY_JSReady);
  if(ready_tuple) {
    // PebbleKit JS is ready! Safe to send messages
    s_js_ready = true;
  }
}

static void inbox_dropped_callback(AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "Message dropped");
}

static void outbox_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "Outbox send failed");
}

static void outbox_sent_callback(DictionaryIterator *iterator, void *context) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Outbox send success");
}

bool app_msg_js_ready() {
  return s_js_ready;
}

void app_msg_init() {
  // Register callbacks
  app_message_register_inbox_received(inbox_received_callback);
  app_message_register_inbox_dropped(inbox_dropped_callback);
  app_message_register_outbox_failed(outbox_failed_callback);
  app_message_register_outbox_sent(outbox_sent_callback);

  // Open AppMessage
  AppMessageResult res = app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
  if (res == APP_MSG_OK) {//TODO handle APP_MSG_OUT_OF_MEMORY error
    APP_LOG(APP_LOG_LEVEL_INFO, "AppMessage opened success");
  }
}

void send_to_phone(app_msg *msg) {
  if (app_msg_js_ready()) {
    // Begin dictionary
    DictionaryIterator *iter;
    AppMessageResult res = app_message_outbox_begin(&iter);
    APP_LOG(APP_LOG_LEVEL_DEBUG, msg->payload);

    if (res == APP_MSG_OK) {  //TODO handle APP_MSG_INVALID_ARGS or APP_MSG_BUSY errors
      // Add a key-value pair
      dict_write_cstring(iter, msg->key, msg->payload);

      // Send the message!
      app_message_outbox_send();
    }
  }
  else {
    save_for_later(msg);
    // start a wakeup timer to try syncing again at a later time, then exit app
    wakeup_cancel_all();
    wakeup_schedule(time(NULL) + 30, 0, false);
    window_stack_pop_all(true);
  }
}
