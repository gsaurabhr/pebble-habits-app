#include <pebble.h>

#ifndef APPMSG
#define APPMSG

typedef struct {
  int key;
  char *payload;
} app_msg;

void app_msg_init();
bool app_msg_js_ready();
void send_to_phone(app_msg*);

#endif // APPMSG
