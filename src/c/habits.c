#include <pebble.h>
#include "appmsg.h"
#include "storage.h"
#include "ui.h"
#include "sync.h"
#include "habits.h"

static bool s_phone_connected = false;
static AppLaunchReason s_launch_reason;

bool phone_connected() {
  return s_phone_connected;
}

static void init(void) {
  storage_init();
  s_phone_connected = connection_service_peek_pebble_app_connection();
  if (s_phone_connected) {
    app_msg_init();
  }

  s_launch_reason = launch_reason();
  if (s_launch_reason == APP_LAUNCH_WAKEUP) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Launched by wakeup. Running sync.");
    sync_init();
  }
  else {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Launched by user. Showing menu.");
    ui_init();
  }
}

static void deinit(void) {
  if (s_launch_reason == APP_LAUNCH_WAKEUP) {
    sync_deinit();
  }
  else {
    ui_deinit();
  }
  storage_deinit();
}

int main(void) {
  init();
  APP_LOG(APP_LOG_LEVEL_INFO, "Done initializing");
  app_event_loop();
  deinit();
}
