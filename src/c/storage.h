#include <pebble.h>
#include "appmsg.h"

#ifndef STORAGE
#define STORAGE

typedef struct {
  uint8_t key; // key for activity
  time_t start_time; // time activity started
  time_t end_time; // time activity ended
} activity;

void storage_init();
void storage_deinit();
void save_for_later(app_msg*);
void load_next_entry(app_msg*);
uint32_t get_unsynced_count();
uint8_t get_running_count();
void save_activity(activity*);
void load_activity(int, activity*, bool);

#endif // STORAGE

/*
Storage formats
1000000 : number of stored strings
1000001 : index of first stored string
*/
