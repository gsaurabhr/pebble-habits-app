#include <pebble.h>
#include "storage.h"
#include "habits.h"

const uint32_t c_persist_number = 1000000; // key to load number of stored strings
const uint32_t c_persist_begin = 1000001; // key to load pointer to first saved string
const uint32_t c_persist_run_number = 2000002; // key to load number of running activities
const uint32_t c_persist_run_begin = 2000003; // key to load pointer to first saved run
static uint32_t s_number_saved = 0;
static uint32_t s_first_index = 0;
static uint32_t s_run_number_saved = 0;
static uint32_t s_run_first_index = 0;
//static activity s_act = {.key=-1, .start_time=-1, .end_time=-1};

void storage_init() {
  if (persist_exists(c_persist_number) & persist_exists(c_persist_begin)) {
    s_number_saved = persist_read_int(c_persist_number);
    s_first_index = persist_read_int(c_persist_begin);
    APP_LOG(APP_LOG_LEVEL_INFO, "Read persist information...\nNumber of saved entries: %lu\nFirst entry at location: %lu", s_number_saved, s_first_index);
  }
  else {
    // create the two storage keys
    APP_LOG(APP_LOG_LEVEL_INFO, "No persistant storage found, creating keys...");
    persist_write_int(c_persist_number, s_number_saved);
    persist_write_int(c_persist_begin, s_first_index);
  }
  if (persist_exists(c_persist_run_number) & persist_exists(c_persist_run_begin)) {
    s_run_number_saved = persist_read_int(c_persist_run_number);
    s_run_first_index = persist_read_int(c_persist_run_begin);
    APP_LOG(APP_LOG_LEVEL_INFO, "Read persist information...\nNumber of running activities: %lu\nFirst entry at location: %lu", s_run_number_saved, s_run_first_index);
  }
  else {
    // create the two storage keys
    APP_LOG(APP_LOG_LEVEL_INFO, "No persistant storage found, creating keys...");
    persist_write_int(c_persist_run_number, s_run_number_saved);
    persist_write_int(c_persist_run_begin, s_run_first_index);
    /*s_act.key = MEDITATION;
    s_act.start_time = time(NULL);
    s_act.end_time = -1;
    save_activity(&s_act);*/
  }
}

void storage_deinit() {
  persist_write_int(c_persist_number, s_number_saved);
  persist_write_int(c_persist_begin, s_first_index);
  persist_write_int(c_persist_run_number, s_run_number_saved);
  persist_write_int(c_persist_run_begin, s_run_first_index);
}

uint8_t get_running_count() {
  return s_run_number_saved;
}

void save_activity(activity *act) {
  uint32_t index = s_run_first_index+s_run_number_saved;
  if (index > 10) {index = index-10;}
  uint32_t buffer[3] = {(uint32_t)act->key, (uint32_t)act->start_time, (uint32_t)act->end_time};
  int suc = persist_write_data(2000004+index, &buffer, sizeof(buffer));
  s_run_number_saved = s_run_number_saved + 1;
  if (s_run_number_saved > 10) {s_run_number_saved = 10;}
  APP_LOG(APP_LOG_LEVEL_INFO, "Saved new activity to persistant storage, with key %lu. Data write returned %d.", index, suc);
}

void load_activity(int i, activity *act, bool del) {
  uint32_t index = s_run_first_index+i;
  if (index > 10) {index = index-10;}
  uint32_t buffer[3];
  persist_read_data(2000004+index, &buffer, sizeof(buffer));
  act->key = (uint8_t)buffer[0];
  act->start_time = (time_t)buffer[1];
  act->end_time = (time_t)buffer[2];
  if (del) {
    persist_delete(2000004+index);
    s_run_number_saved = s_run_number_saved - 1;
    s_run_first_index = s_run_first_index + 1;
    if (s_first_index == 10) {s_first_index = 0;}
  }
}

void save_for_later(app_msg *msg) {
  uint32_t index = s_first_index+s_number_saved;
  if (index > 999999) {index = index-1000000;}
  persist_write_int(c_persist_begin+1+index, msg->key);
  int suc = persist_write_string(index, msg->payload);
  s_number_saved = s_number_saved + 1;
  APP_LOG(APP_LOG_LEVEL_INFO, "Saved new string to persistant storage, with keys %lu, %lu. String write returned %d.", c_persist_begin+1+index, index, suc);
}

void load_next_entry(app_msg *msg) {
  if (s_number_saved > 0) {
    uint32_t index = s_first_index;
    if (index > 999999) {index = index-1000000;}
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Reading keys %lu, %lu.", index, c_persist_begin+1+index);
    msg->key = persist_read_int(c_persist_begin+1+index);persist_read_string(index, msg->payload, persist_get_size(index));
    APP_LOG(APP_LOG_LEVEL_DEBUG, msg->payload);
    persist_delete(index);
    persist_delete(c_persist_begin+1+index);
    s_number_saved = s_number_saved - 1;
    s_first_index = s_first_index + 1;
    if (s_first_index == 1000000) {s_first_index = 0;}
  }
  else {
    msg->key = -1;
    APP_LOG(APP_LOG_LEVEL_INFO, "No stored data.");
  }
}

uint32_t get_unsynced_count() {
  return s_number_saved;
}
