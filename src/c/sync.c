#include <pebble.h>
#include "habits.h"
#include "sync.h"
#include "appmsg.h"
#include "storage.h"

static Window *s_window;
static TextLayer *s_text_layer;
static uint32_t s_init_unsynced = 0;
static app_msg s_msg = {.key=-1, .payload=""};
static char s_screen_str[200];
static AppTimer *s_js_timer;

static void start_sync();

static void cb_js_ready_timer(void *data) {
  if (app_msg_js_ready()) {
      start_sync();
    }
  else {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Waiting for pkjs ready signal.");
    s_js_timer = app_timer_register(400, cb_js_ready_timer, NULL);
  }
}

static void start_sync() {
  for (int i=0; i<(int)s_init_unsynced; i++) {
    snprintf(s_screen_str, sizeof(s_screen_str), "Synchronizing stored logs...\n%d/%lu completed.", i, s_init_unsynced);
    text_layer_set_text(s_text_layer, s_screen_str);
    load_next_entry(&s_msg);
    if (s_msg.key >= 0) {
      APP_LOG(APP_LOG_LEVEL_INFO, "Loaded entry at %u:", s_msg.key);
      APP_LOG(APP_LOG_LEVEL_INFO, s_msg.payload);
      send_to_phone(&s_msg);
    }
    //psleep(1000);
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done syncing.");
  window_stack_pop_all(true);
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_text_layer = text_layer_create(GRect(0, 72, bounds.size.w, 100));
  text_layer_set_text(s_text_layer, "Sync logs...");
  text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
  text_layer_set_overflow_mode(s_text_layer, GTextOverflowModeWordWrap);
  layer_add_child(window_layer, text_layer_get_layer(s_text_layer));
}

static void window_unload(Window *window) {
  text_layer_destroy(s_text_layer);
}

void sync_init() {
  if (phone_connected()) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Phone connected.");
    s_window = window_create();
    window_set_click_config_provider(s_window, click_config_provider);
    window_set_window_handlers(s_window, (WindowHandlers) {
      .load = window_load,
      .unload = window_unload,
    });
    const bool animated = true;
    window_stack_push(s_window, animated);
    s_init_unsynced = get_unsynced_count();
    snprintf(s_screen_str, sizeof(s_screen_str), "Sync %lu logs...\nWaiting for phone.", s_init_unsynced);
    text_layer_set_text(s_text_layer, s_screen_str);
    if (app_msg_js_ready()) {
      start_sync();
    }
    else {
      APP_LOG(APP_LOG_LEVEL_DEBUG, "Waiting for pkjs ready signal.");
      s_js_timer = app_timer_register(400, cb_js_ready_timer, NULL);
    }
  }
  else {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Phone not connected. Cancelling old wakeups and setting up a new one.");
    wakeup_cancel_all();
    wakeup_schedule(time(NULL) + 30, 0, false);
  }
}

void sync_deinit() {
  if (phone_connected()) {
    window_destroy(s_window);
  }
}
