#include <pebble.h>

#ifndef HABITS
#define HABITS

#define EXERCISE 0;
#define SPORTS 1;
#define READING 2;
#define MEDITATION 3;
#define TRASH 4;

static const char* act_names[5] = {"Exercise", "Sports", "Reading", "Meditation", "Trash"};

bool phone_connected();

#endif // HABITS
