#include <pebble.h>

#ifndef SYNC
#define SYNC

#define wakeup_id_sync 0

void sync_init();
void sync_deinit();

#endif // SYNC
