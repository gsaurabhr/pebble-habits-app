module.exports = [
  {
    "type": "heading",
    "defaultValue": "Habits App Configuration"
  },
  {
    "type": "text",
    "defaultValue": "Please create an account on the adafruit.io page and locate the AIO key."
  },
  {
    "type": "section",
    "items": [
      {
        "type": "heading",
        "defaultValue": "Adafruit AIO Key"
      },
      {
        "type": "input",
        "messageKey": "AdafruitKey",
        "id": "aio",
        "defaultValue": "",
        "label": "Key"
      }
    ]
  },
  {
    "type": "submit",
    "defaultValue": "Save Settings"
  }
];
