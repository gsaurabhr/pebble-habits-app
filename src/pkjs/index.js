// Import the Clay package
var Clay = require('pebble-clay');
// Load our Clay configuration file
var clayConfig = require('./config');
// Initialize Clay
var clay = new Clay(clayConfig);

var settings = JSON.parse(localStorage.getItem('clay-settings'))
var aio_key = settings['AdafruitKey'];

// Send data string to adafruit.io
function send_to_ada(msg) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', `https://io.adafruit.com/api/v2/gsaurabhr/feeds/pebblehabits/data?X-AIO-Key=${aio_key}`, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('accept', 'application/json');
  xhr.setRequestHeader('X-AIO-Key', aio_key);
  xhr.onload = function () {
      // do something to response
      console.log(this.responseText);
  };
  xhr.ontimeout = function () {
      // do something to response
      console.log("Timeout!");
  };
  xhr.send(`{"value":"${msg}"}`);
}

// Listen for when the watchface is opened
Pebble.addEventListener('ready', function(e) {
    console.log('PebbleKit JS ready!');
    // Update s_js_ready on watch
    Pebble.sendAppMessage({'JSReady': 1});
  }
);

// Listen for when an AppMessage is received
Pebble.addEventListener('appmessage', function(e) {
    console.log('AppMessage received!');
    //console.log(Object.keys(driversCounter));
    if (e.payload['ACTIVITY']) {
      data = e.payload['ACTIVITY']
      console.log(data);
      send_to_ada(data);
    }
  }                     
);
